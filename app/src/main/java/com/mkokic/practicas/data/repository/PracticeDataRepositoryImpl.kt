package com.mkokic.practicas.data.repository

import com.mkokic.practicas.domain.PracticeData
import com.mkokic.practicas.domain.PracticeRepository

class PracticeDataRepositoryImpl : PracticeRepository {

    override fun getPracticeData(): PracticeData {
        return PracticeData("Práctica 03")
    }

    override fun addPracticeData(practiceData: PracticeData) {

    }

    override fun deletePracticeData() {
    }

    override fun updatePracticeData(practiceData: PracticeData) {
    }
}