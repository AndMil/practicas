package com.mkokic.practicas.domain.usecases

import com.mkokic.practicas.domain.PracticeData
import com.mkokic.practicas.domain.PracticeRepository

class AddPracticeData(private val practiceRepository: PracticeRepository) {

    fun addPracticeData(practiceData: PracticeData) {
        return practiceRepository.addPracticeData(practiceData)
    }
}