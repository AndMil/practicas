package com.mkokic.practicas.domain.usecases

import com.mkokic.practicas.domain.PracticeData
import com.mkokic.practicas.domain.PracticeRepository

class GetPracticeData(private val practiceRepository: PracticeRepository) {

    fun getPracticeData(): PracticeData {
        return practiceRepository.getPracticeData()
    }
}