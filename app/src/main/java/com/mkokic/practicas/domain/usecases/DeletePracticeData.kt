package com.mkokic.practicas.domain.usecases

import com.mkokic.practicas.domain.PracticeRepository

class DeletePracticeData(private val practiceRepository: PracticeRepository) {

    fun deletePracticeData() {
        return practiceRepository.deletePracticeData()
    }
}