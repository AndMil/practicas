package com.mkokic.practicas.domain.usecases

import com.mkokic.practicas.domain.PracticeData
import com.mkokic.practicas.domain.PracticeRepository

class UpdatePracticeData(private val practiceRepository: PracticeRepository) {

    fun updatePracticeData(practiceData: PracticeData) {
        return practiceRepository.updatePracticeData(practiceData)
    }
}