package com.mkokic.practicas.presentation.view

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.material3.Surface
import com.mkokic.practicas.presentation.viewmodel.HomeViewModel
import com.mkokic.practicas.presentation.viewmodel.HomeViewModelFactory

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Surface {
                val factory = HomeViewModelFactory()
                val viewModel= factory.create(HomeViewModel::class.java)
                HomeScreen(viewModel = viewModel)
            }
        }
    }
}
