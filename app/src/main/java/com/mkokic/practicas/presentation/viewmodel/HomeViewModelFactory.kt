package com.mkokic.practicas.presentation.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.mkokic.practicas.data.repository.PracticeDataRepositoryImpl
import com.mkokic.practicas.domain.usecases.AddPracticeData
import com.mkokic.practicas.domain.usecases.DeletePracticeData
import com.mkokic.practicas.domain.usecases.GetPracticeData
import com.mkokic.practicas.domain.usecases.UpdatePracticeData

class HomeViewModelFactory() : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        val practiceDataRepositoryImpl = PracticeDataRepositoryImpl()
        return HomeViewModel(
            getPracticeData = GetPracticeData(practiceDataRepositoryImpl),
            addPracticeData = AddPracticeData(practiceDataRepositoryImpl),
            deletePracticeData = DeletePracticeData(practiceDataRepositoryImpl),
            updatePracticeData = UpdatePracticeData(practiceDataRepositoryImpl)
        ) as T
    }
}
