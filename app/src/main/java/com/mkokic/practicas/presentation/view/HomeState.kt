package com.mkokic.practicas.presentation.view

import com.mkokic.practicas.domain.PracticeData


sealed class HomeState {
    object Loading : HomeState()
    data class Success(val practiceData: PracticeData) : HomeState()
    data class Failure(val exception: Throwable) : HomeState()
}